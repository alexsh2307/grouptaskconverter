package task;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Класс, являющийся точкой входа
 *
 * @author Шашков Александр
 */
public class Main extends Application {

    /**
     *Создает окно интерфейса
     *
     * @param primaryStage объект окна
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Конвертер");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    /**
     *точка входа в программу
     *
     * @param args предоставленные аргументы командной строки
     */
    public static void main(String[] args) {
        launch(args);
    }
}
