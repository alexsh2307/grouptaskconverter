package task.size;

/**
 * Класс модели столбца таблицы с размерами всех систем
 *
 * @author Шашков Александр, Челноков Егор
 */
public class Size {
    private final GapSize russianSize;// русский размер
    private final String americanSize;// американский/международный размер
    private final GapSize germanySize;// германский размер
    private final GapSize frenchSize;// французский/итальянский размер
    private final GapSize englandSize;// британский размер

    /*конструктор столбца*/
    public Size(GapSize russianSize, String americanSize, GapSize germanySize, GapSize frenchSize, GapSize englandSize) {
        this.russianSize = russianSize;
        this.americanSize = americanSize;
        this.germanySize = germanySize;
        this.frenchSize = frenchSize;
        this.englandSize = englandSize;
    }

    public GapSize getRussianSize() {
        return russianSize;
    }

    public String getAmericanSize() {
        return americanSize;
    }

    public GapSize getGermanySize() {
        return germanySize;
    }

    public GapSize getFrenchSize() {
        return frenchSize;
    }

    public GapSize getEnglandSize() {
        return englandSize;
    }

    /**
     * возвращает размер в виде промежутка по названию его системы
     * (не считая американской системы)
     *
     * @param name название размерной системы
     * @return gapSize - промежуток нужной размерной системы
     */
    public GapSize getBySystemName(String name){
        GapSize gapSize = null;
        switch (name) {
            case "Россия" :
                gapSize = getRussianSize();
                break;
            case "Германия" :
                gapSize = getGermanySize();
                break;
            case "Франция, Италия":
                gapSize = getFrenchSize();
                break;
            case "Великобритания":
                gapSize = getEnglandSize();
                break;
        }
        return gapSize;
    }

}
