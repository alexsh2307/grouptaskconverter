package task.size;

/**
 * Класс модели столбца таблицы с размерами всех систем
 *
 * @author Лучинкин Глеб
 */

class ShoeSize {

    private float russianSize;
    private float europeSize;
    private float englandSize;
    private float usaSize;

    /*конструктор столбца*/
   public ShoeSize (float russianSize, float europeSize, float englandSize, float usaSize){
        this.russianSize = russianSize;
        this.europeSize = europeSize;
        this.englandSize = englandSize;
        this.usaSize = usaSize;
    }

    private float getRussianSize() {
        return russianSize;
    }
    private float getEuropeSize() {
        return europeSize;
    }
    private float getEnglandSize() {
        return englandSize;
    }
    private float getUsaSize() {
        return usaSize;
    }


    /**
     * возвращает размер в виде строки по названию его системы
     *
     * @param name название размерной системы
     * @return shoeSize - размер обуви нужно системы
     */

   public String getBySystemName(String name){
        float shoeSize;
        switch (name) {
            case "Россия" :
                shoeSize = getRussianSize();
                break;
            case "Европа" :
                shoeSize = getEuropeSize() ;
                break;
            case "США":
                shoeSize = getUsaSize();
                break;
            case "Великобритания":
                shoeSize = getEnglandSize();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + name);
        }
        return String.valueOf(shoeSize);
    }

}
