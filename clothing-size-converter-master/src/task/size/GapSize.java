package task.size;

/**
 * Класс модели ячейки таблицы и размера в виде промежутка
 * (для размеров всех систем, кроме американской)
 *
 * @author Шашков Александр, Челноков Егор
 */

public class GapSize {
    private final int min;//начало промежутка, минимум
    private final int max;//конец промежутка, максимум

    /*конструктор для промежутка*/
    public GapSize(int min, int max) {
        this.min = min;
        this.max = max;
    }

    /*конструктор для одной величины*/
    public GapSize(int value) {
        this.min = value;
        this.max = value;
    }

    /**
     * проверяет, входит ли размер в текущий промежуток
     * @param size величина размера
     * @return true если величина входит в промежуток
     * или false если не входит
     */
    public boolean isInGap(int size) {
        return min <= size && max >= size;
    }

    /**
     * возвращает минимальное значение промежутка
     * @return минимальное значение промежутка
     */
    public int getMin() {
        return min;
    }

    @Override
    public String toString() {
        return min +
                "-" + max;
    }
}
