package task.size;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, конвертирующий размеры обуви из одной системы в другую
 *
 * @author Лучинкин Глеб
 */

public class ShoesConverter {

    /*Список мужских размеров обуви*/
    private static ArrayList<ShoeSize> maleList = new ArrayList<>(List.of(
            new ShoeSize(35, 36, 4,4.5f),
            new ShoeSize(36, 37, 4.5f,5),
            new ShoeSize(36.5f, 37.5f, 5,5.5f),
            new ShoeSize(37, 38, 5.5f,6),
            new ShoeSize(37.5f, 38.5f, 6,6.5f),
            new ShoeSize(38, 39, 6.5f,7),
            new ShoeSize(39, 40, 7,7.5f),
            new ShoeSize(40, 41, 7.5f,8),
            new ShoeSize(41, 42, 8,8.5f),
            new ShoeSize(42, 43, 8.5f,9),
            new ShoeSize(43, 44, 10,10.5f),
            new ShoeSize(44, 45, 11,11.5f),
            new ShoeSize(45, 46, 12,12.5f),
            new ShoeSize(46, 47, 13,14)
    ));
    /*Список женских размеров обуви*/
    private static ArrayList<ShoeSize> femaleList = new ArrayList<>(List.of(
            new ShoeSize(34, 35, 2.5f,5),
            new ShoeSize(34.5f, 35.5f, 3,5.5f),
            new ShoeSize(35, 36, 3.5f,6),
            new ShoeSize(36, 37, 4,6.5f),
            new ShoeSize(36.5f, 37.5f, 4.5f,7),
            new ShoeSize(37, 38, 5,7.5f),
            new ShoeSize(37.5f, 38.5f, 5.5f,8),
            new ShoeSize(38, 39, 6,8.5f),
            new ShoeSize(39, 40, 6.5f,9),
            new ShoeSize(40, 41, 7,9.5f),
            new ShoeSize(41, 42, 7.5f,10),
            new ShoeSize(42, 43, 8,10.5f),
            new ShoeSize(43, 44, 9.5f,12),
            new ShoeSize(44, 45, 10.5f,13)
    ));

    /**
     * возвращает конвертированный размер из исходной системы в конечную
     * в виде строковой величины
     *
     * @param source название исходной системы
     * @param target название конечной системы
     * @param value  величина размера
     * @param isMale показывает, использовать таблицу мужских размеров или женских
     * @return размер в виде строковой величины или сообщение "NOT FOUND"
     */

    public static String convert(String source, String target, String value, boolean isMale) {

        ShoeSize found;
        if (isMale) {
            found = getSize(source, value, maleList);
        } else {
            found = getSize(source, value, femaleList);
        }
        if (found != null) {
            return found.getBySystemName(target);
        } else {
            return "NOT FOUND";
        }
    }

    /**
     * возвращает искомый столбец в таблице по заданному размеру
     * @param source название используемой размерной системы
     * @param value величина размера
     * @param sizeList используемая таблица размеров
     * @return найденный размерный столбец
     */

    private static ShoeSize getSize(String source, String value, ArrayList<ShoeSize> sizeList) {
        ShoeSize found = null;
        for (ShoeSize shoeSize : sizeList) {
            if (Float.parseFloat(shoeSize.getBySystemName(source)) == Float.parseFloat(value)) {
                found = shoeSize;
                break;
            }
        }
        return found;
    }
}

