package task;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import task.size.ShoesConverter;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Класс, связывающий frontend и backend
 *
 * @author Шашков Александр
 */
public class Controller implements Initializable {

    /*Элементы интерфейса*/

    /*Элементы, обрабатывающие мужскую одежду*/

    @FXML
    private ChoiceBox<String> systemSizeFromConvertMenClothes;

    @FXML
    private ChoiceBox<String> systemSizeToConvertMenClothes;

    @FXML
    private ChoiceBox<String> valueSizeInStringMenClothes;

    @FXML
    private TextField valueSizeInNumberMenClothes;

    @FXML
    private Label resultSizeMenClothes;

    /*Элементы, обрабатывающие женскую одежду*/

    @FXML
    private ChoiceBox<String> systemSizeFromConvertWomenClothes;

    @FXML
    private ChoiceBox<String> systemSizeToConvertWomenClothes;

    @FXML
    private ChoiceBox<String> valueSizeInStringWomenClothes;

    @FXML
    private TextField valueSizeInNumberWomenClothes;

    @FXML
    private Label resultSizeWomenClothes;

    /*Элементы, обрабатывающие мужскую обувь*/

    @FXML
    private ChoiceBox<String> systemSizeFromConvertMenShoes;

    @FXML
    private ChoiceBox<String> systemSizeToConvertMenShoes;

    @FXML
    private TextField valueSizeInNumberMenShoes;

    @FXML
    private Label resultSizeMenShoes;

    /*Элементы, обрабатывающие женскую обувь*/

    @FXML
    private ChoiceBox<String> systemSizeFromConvertWomenShoes;

    @FXML
    private ChoiceBox<String> systemSizeToConvertWomenShoes;

    @FXML
    private TextField valueSizeInNumberWomenShoes;

    @FXML
    private Label resultSizeWomenShoes;

    /*Константные названия систем размеров одежды*/

    private static final String[] SYSTEMS_SIZES_CLOTHES = {
            "Россия",
            "Америка",
            "Германия",
            "Франция, Италия",
            "Великобритания"
    };

    /*Константные названия систем размеров обуви*/

    private static final String[] SYSTEMS_SIZES_SHOES = {
            "Россия",
            "Европа",
            "Великобритания",
            "США"
    };

    /*Константные значения американских размеров одежды*/

    private static final String[] VALUE_AMERICAN_SIZES_CLOTHES = {
            "XS",
            "S",
            "M",
            "L",
            "XL",
            "XXL",
            "XXXL"
    };

    /**
     * инициализация объектов интерфейса
     *
     * @param url местоположение, используемое для определения относительных путей для корневого объекта
     * @param resourceBundle ресурсы, используемые для локализации корневого объекта
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        initializeClothes(systemSizeFromConvertMenClothes, systemSizeToConvertMenClothes, valueSizeInStringMenClothes, valueSizeInNumberMenClothes, resultSizeMenClothes, true);
        initializeClothes(systemSizeFromConvertWomenClothes, systemSizeToConvertWomenClothes, valueSizeInStringWomenClothes, valueSizeInNumberWomenClothes, resultSizeWomenClothes, false);

        initialiseShoes(systemSizeFromConvertMenShoes, systemSizeToConvertMenShoes, valueSizeInNumberMenShoes, resultSizeMenShoes, true);
        initialiseShoes(systemSizeFromConvertWomenShoes, systemSizeToConvertWomenShoes, valueSizeInNumberWomenShoes, resultSizeWomenShoes, false);
    }

    /**
     * инициализация объектов интерфейса конкретными значениями для конвертации размеров одежды
     *
     * @param choiceBoxSystemFrom список систем, из которых осуществляется конвертация
     * @param choiceBoxSystemTo список систем, в которые осуществляется конвертация
     * @param choiceBoxValue значения американских размеров
     * @param textFieldValue поле ввода размеров любой системы, кроме американских
     * @param labelResult поле вывода результата
     * @param isMaleGender обозначение пола одежды
     */
    private void initializeClothes(ChoiceBox<String> choiceBoxSystemFrom, ChoiceBox<String> choiceBoxSystemTo, ChoiceBox<String> choiceBoxValue, TextField textFieldValue, Label labelResult, boolean isMaleGender) {
        ObservableList<String> items = FXCollections.observableArrayList(SYSTEMS_SIZES_CLOTHES);
        choiceBoxSystemFrom.setItems(items);
        choiceBoxSystemFrom.setValue(SYSTEMS_SIZES_CLOTHES[0]);

        setValue(choiceBoxSystemFrom, choiceBoxSystemTo, SYSTEMS_SIZES_CLOTHES);
        choiceBoxSystemTo.setValue(SYSTEMS_SIZES_CLOTHES[1]);

        ObservableList<String> itemsValues = FXCollections.observableArrayList(VALUE_AMERICAN_SIZES_CLOTHES);
        choiceBoxValue.setItems(itemsValues);
        choiceBoxValue.setValue(VALUE_AMERICAN_SIZES_CLOTHES[0]);


        choiceBoxSystemFrom.setOnAction(event -> setVisibleAndValue(
                choiceBoxSystemFrom,
                choiceBoxSystemTo,
                choiceBoxValue,
                textFieldValue));

        textFieldValue.focusedProperty().addListener((arg0, oldValue, newValue) -> checkAndConvert(
                newValue,
                choiceBoxSystemFrom,
                choiceBoxSystemTo,
                textFieldValue,
                labelResult,
                34,
                64,
                isMaleGender, true));

        choiceBoxValue.visibleProperty().addListener((arg0, oldValue, newValue) -> {
            if (newValue) {
                setResult(
                        convert(choiceBoxValue.getValue(),
                                choiceBoxSystemFrom.getValue(),
                                choiceBoxSystemTo.getValue(),
                                isMaleGender, true),
                                labelResult);
            }
        });

        choiceBoxValue.valueProperty().addListener(
                (arg0, oldValue, newValue) -> setResult(
                        convert(choiceBoxValue.getValue(),
                                choiceBoxSystemFrom.getValue(),
                                choiceBoxSystemTo.getValue(),
                                isMaleGender, true),
                                labelResult));
    }

    /**
     * инициализация объектов интерфейса конкретными значениями для конвертации размеров обуви
     *
     * @param choiceBoxSystemFrom список систем, из которых осуществляется конвертация
     * @param choiceBoxSystemTo список систем, в которые осуществляется конвертация
     * @param textFieldValue поле ввода размеров любой системы
     * @param labelResult поле вывода результата
     * @param isMaleGender обозначение пола обуви
     */
    private void initialiseShoes(ChoiceBox<String> choiceBoxSystemFrom, ChoiceBox<String> choiceBoxSystemTo, TextField textFieldValue, Label labelResult, boolean isMaleGender) {
        ObservableList<String> items = FXCollections.observableArrayList(SYSTEMS_SIZES_SHOES);
        choiceBoxSystemFrom.setItems(items);
        choiceBoxSystemFrom.setValue(SYSTEMS_SIZES_SHOES[0]);

        setValue(choiceBoxSystemFrom, choiceBoxSystemTo, SYSTEMS_SIZES_SHOES);
        choiceBoxSystemTo.setValue(SYSTEMS_SIZES_SHOES[1]);

        choiceBoxSystemFrom.setOnAction(event -> setValue(
                choiceBoxSystemFrom,
                choiceBoxSystemTo,
                SYSTEMS_SIZES_SHOES));

        textFieldValue.focusedProperty().addListener((arg0, oldValue, newValue) -> checkAndConvert(
                newValue,
                choiceBoxSystemFrom,
                choiceBoxSystemTo,
                textFieldValue,
                labelResult,
                5,
                47,
                isMaleGender, false));
    }

    /**
     * задает видимость и значения объектов интерфейса
     *
     * @param choiceBoxSystemFrom список систем, из которых осуществляется конвертация
     * @param choiceBoxSystemTo список систем, в которые осуществляется конвертация
     * @param choiceBoxValue значения американских размеров
     * @param textFieldValue поле ввода размеров любой системы, кроме американских
     */
    private void setVisibleAndValue(ChoiceBox<String> choiceBoxSystemFrom, ChoiceBox<String> choiceBoxSystemTo, ChoiceBox<String> choiceBoxValue, TextField textFieldValue) {
        setVisibly(choiceBoxSystemFrom,choiceBoxValue, textFieldValue);
        setValue(choiceBoxSystemFrom, choiceBoxSystemTo, SYSTEMS_SIZES_CLOTHES);
    }

    /**
     * задает видимость объектов интерфейса
     *
     * @param choiceBoxSystemFrom список систем, из которых осуществляется конвертация
     * @param choiceBoxValue значения американских размеров
     * @param textFieldValue поле ввода размеров любой системы, кроме американских
     */
    private void setVisibly(ChoiceBox<String> choiceBoxSystemFrom, ChoiceBox<String> choiceBoxValue, TextField textFieldValue) {
        if (choiceBoxSystemFrom.getSelectionModel().getSelectedItem().equals("Америка")) {
            choiceBoxValue.setVisible(true);
            textFieldValue.setVisible(false);
        } else {
            choiceBoxValue.setVisible(false);
            textFieldValue.setVisible(true);
        }
    }

    /**
     * задает значения объектов интерфейса
     *
     * @param choiceBoxSystemFrom список систем, из которых осуществляется конвертация
     * @param choiceBoxSystemTo список систем, в которые осуществляется конвертация
     * @param systemSize список систем
     */
    private void setValue(ChoiceBox<String> choiceBoxSystemFrom, ChoiceBox<String> choiceBoxSystemTo, String[] systemSize) {
        ObservableList<String> items = FXCollections.observableArrayList();

        for (int i = 0; i < systemSize.length; i++) {
            if (choiceBoxSystemFrom.getValue().equals(systemSize[i])) {
                for (int j = 0; j < systemSize.length; j++) {
                    if (i != j) {
                        items.add(systemSize[j]);
                    }
                }
                choiceBoxSystemTo.setItems(items);
                choiceBoxSystemTo.setValue(items.get(0));
                break;
            }
        }
    }

    /**
     * проверка ввода пользователем значений и вызов метода конвертации
     *
     * @param newValue проверка на изменение значения текстового поля ввода
     * @param choiceBoxSystemFrom список систем, из которых осуществляется конвертация
     * @param choiceBoxSystemTo список систем, в которые осуществляется конвертация
     * @param textFieldValue поле ввода размеров любой системы, кроме американских
     * @param labelResult поле вывода результата
     * @param minSize макисмальный возможный размер всей таблицы
     * @param maxSize минимальный возможный размер всей таблицы
     * @param isMaleGender обозначение пола одежды или обуви
     * @param clothesOrShoes обозначение одежда/обувь
     */
    private void checkAndConvert(boolean newValue, ChoiceBox<String> choiceBoxSystemFrom, ChoiceBox<String> choiceBoxSystemTo, TextField textFieldValue, Label labelResult, int minSize, int maxSize, boolean isMaleGender, boolean clothesOrShoes) {

        if (!newValue) {
            if (textFieldValue.getText().matches("(\\d+)|(\\d+.\\.\\d+)")) {
                if (Double.parseDouble(textFieldValue.getText()) <= maxSize && Double.parseDouble(textFieldValue.getText()) >= minSize) {
                    setResult(convert(textFieldValue.getText(),
                            choiceBoxSystemFrom.getValue(),
                            choiceBoxSystemTo.getValue(),
                            isMaleGender, clothesOrShoes), labelResult);
                } else {
                    setError("You number is not a size. You enter " + textFieldValue.getText());
                }
            } else {
                setError("You didn't enter an integer. You enter " + textFieldValue.getText());
            }
        }
    }

    /**
     * возвращает конвертированный размер из исходной системы в конечную
     * в виде строковой величины
     *
     * @param value значение размера
     * @param systemFromConvert список систем, из которых осуществляется конвертация
     * @param systemToConvert список систем, в которые осуществляется конвертация
     * @param isMaleGender обозначение пола одежды или обуви
     * @param isClothes обозначение одежда/обувь
     */
    private String convert(String value, String systemFromConvert, String systemToConvert, boolean isMaleGender, boolean isClothes) {
        String result;
        if (isClothes){
            result = Converter.convert(systemFromConvert, systemToConvert, value, isMaleGender);
        }else{
            result = ShoesConverter.convert(systemFromConvert, systemToConvert, value, isMaleGender);
        }
        return result;
    }

    /**
     * выводит в поле вывода результата конвертированное значение
     *
     * @param value значение размера
     * @param labelResult поле вывода результата
     */
    private void setResult(String value, Label labelResult) {
        labelResult.setText(value);
    }

    /**
     * вызов окна с сообщением об ошибке
     *
     * @param messageError сообщение об ошибке
     */
    private static void setError(String messageError) {
        Alert alert = new Alert(Alert.AlertType.ERROR);

        alert.setTitle("ERROR");
        alert.setHeaderText("Look, an Error Dialog");
        alert.setContentText(messageError);

        alert.showAndWait();
    }
}
