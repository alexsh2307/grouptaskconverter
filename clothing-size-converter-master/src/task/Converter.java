package task;

import task.size.GapSize;
import task.size.Size;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, конвертирующий размеры из одной системы в другую
 *
 * @author Челноков Е.И.
 */
public class Converter {
    /*Список мужских размеров*/
    static ArrayList<Size> maleList = new ArrayList<>(List.of(
            new Size(new GapSize(44, 46), "XS", new GapSize(42, 44), new GapSize(40, 44), new GapSize(34, 35)),
            new Size(new GapSize(47, 48), "S", new GapSize(45, 48), new GapSize(45, 48), new GapSize(36, 38)),
            new Size(new GapSize(49, 50), "M", new GapSize(49, 52), new GapSize(49, 52), new GapSize(39, 41)),
            new Size(new GapSize(51, 52), "L", new GapSize(53, 56), new GapSize(53, 56), new GapSize(42, 44)),
            new Size(new GapSize(53, 54), "XL", new GapSize(56, 59), new GapSize(57, 58), new GapSize(45, 47)),
            new Size(new GapSize(55, 56), "XXL", new GapSize(60), new GapSize(59, 60), new GapSize(48, 49)),
            new Size(new GapSize(57, 58), "XXXL", new GapSize(61), new GapSize(61), new GapSize(50))));

    /*Список женских размеров*/
    static ArrayList<Size> femaleList = new ArrayList<>(List.of(
            new Size(new GapSize(40), "XS", new GapSize(32, 34), new GapSize(34, 36), new GapSize(6, 8)),
            new Size(new GapSize(42), "S", new GapSize(35, 36), new GapSize(38, 40), new GapSize(9, 10)),
            new Size(new GapSize(44, 46), "M", new GapSize(38, 40), new GapSize(41, 42), new GapSize(11, 12)),
            new Size(new GapSize(48), "L", new GapSize(41, 42), new GapSize(44, 46), new GapSize(14, 16)),
            new Size(new GapSize(50, 52), "XL", new GapSize(44, 46), new GapSize(47, 48), new GapSize(17, 18)),
            new Size(new GapSize(54), "XXL", new GapSize(47, 48), new GapSize(50, 52), new GapSize(19, 22)),
            new Size(new GapSize(56), "XXXL", new GapSize(49, 50), new GapSize(53, 55), new GapSize(23, 25))));

    /**
     * возвращает конвертированный размер из исходной системы в конечную
     * в виде строковой величины
     *
     * @param source название исходной системы
     * @param target название конечной системы
     * @param value  величина размера
     * @param isMale показывает, использовать таблицу мужских размеров или женских
     * @return размер в виде строковой величины или сообщение "NOT FOUND"
     */
    public static String convert(String source, String target, String value, boolean isMale) {
        //для дальнейшей работы с американским размером, он предварительно конвертируется в русский
        if (source.equals("Америка")) {
            source = "Россия";
            value = String.valueOf(
                    getByAmericanSize(value)
                            .getRussianSize()
                            .getMin()
            );
        }

        Size found;
        /*переключатель таблиц мужских и женских размеров*/
        if (isMale) {
            found = getSize(source, value, maleList);
        } else {
            found = getSize(source, value, femaleList);
        }
        if (found != null) {
            //конвертация в американскую систему идёт отдельно от остальных
            if (target.equals("Америка")) {
                return found.getAmericanSize();
            } else {
                return found.getBySystemName(target).toString();
            }
        } else {
            return "NOT FOUND";
        }
    }

    /**
     * возвращает искомый столбец в таблице по заданному размеру
     *
     * @param source   название используемой размерной системы
     * @param value    величина размера
     * @param sizeList используемая таблица размеров
     * @return найденный размерный столбец
     */
    private static Size getSize(String source, String value, ArrayList<Size> sizeList) {
        Size found = null;
        for (Size size : sizeList) {
            if (size.getBySystemName(source).isInGap(Integer.parseInt(value))) {
                found = size;
                break;
            }
        }
        return found;
    }

    /**
     * возвращает искомый столбец таблицы по величине его американского размера
     *
     * @param value величина американского размера
     * @return найденный столбец или null
     */
    private static Size getByAmericanSize(String value) {
        Size found = null;
        for (Size size : maleList) {
            if (size.getAmericanSize().equals(value)) {
                found = size;
            }
        }
        return found;
    }
}
